package co.redeye.core;

/**
 * Created on 1/09/2015.
 */
public class DocumentScrapeCli {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: java -jar DocumentScrape-1.0-SNAPSHOT.jar /path/to/file.name");
            return;
        }
        String pathName = args[0];
        AlacerCoverPageScrape alacerCoverPageScrape = new AlacerCoverPageScrape();
        try {
            System.out.println(alacerCoverPageScrape.parseAlacerDoc(pathName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
