package co.redeye.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 31/08/2015.
 */
public class AlacerCoverPageScrape extends DocumentScrape {

    private static final String COMPANY = "Alacer";

    class CoverPage {
        public String pathName;
        public String company;
        public Map<String, String> attributeMap;
        public List<Revision> revisions;

        public CoverPage(String pathName, String company) {
            this.pathName = pathName;
            this.company = company;
            attributeMap = new HashMap<String, String>();
            revisions = new ArrayList<Revision>();
        }
    }

    class Revision {
        public String number;
        public String date;
        public String comments;

        public Revision(String number, String date, String comments) {
            this.number = number;
            this.date = date;
            this.comments = comments;
        }
    }

    /**
     * Parse a text file, line by line.
     * If line contains a colon : then assume key to the left and
     * value to the right.
     *
     * e.g.
     * Name : George
     *
     * "Name" -> "George"
     *
     *
     * @param fileName
     * @return
     */
    public String parseAlacerDoc(final String fileName) throws IOException, SAXException, TikaException {
        CoverPage coverPage = new CoverPage(fileName, COMPANY);
        String plainText = getTikaAutoParse(fileName);
        String[] lines = plainText.split("\n\n");
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i].trim();
            if (line.length() > 0) {
                String[] bits = line.split("\n");
                if (bits.length == 2) { // must be key value with :
                    coverPage.attributeMap.put(bits[0].trim(), bits[1].trim());
                } else if (bits.length == 3) { // must be a revision record
                    String number = bits[0].trim();
                    String date = bits[1].trim();
                    String comments = bits[2].trim();
                    if (number.charAt(0) != 8194) {
                        Revision revision = new Revision(number, date, comments);
                        coverPage.revisions.add(revision);
                    }
                } else {
                    throw new RuntimeException("Invalid format.");
                }
            }
        }
        return convertToJson(coverPage);
    }

    public String convertToJson(final CoverPage coverPage) {
//        System.out.println("************** Begin Dump ****************");
//        dumpAttributes(coverPage.attributeMap);
//        dumpRevisions(coverPage.revisions);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(coverPage);
        return json;
    }

    private void dumpRevisions(List<Revision> revisions) {
        for (Revision revision : revisions) {
            System.out.println(revision.number + ", " + revision.date + ", " + revision.comments);
        }
    }

    private void dumpAttributes(Map<String, String> attributeMap) {
        for (Map.Entry<String, String> entry : attributeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + " => " + value);
        }
    }

}
