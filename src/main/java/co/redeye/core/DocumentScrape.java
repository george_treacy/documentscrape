package co.redeye.core;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DocumentScrape {

    public String getTextFromDocx(final String fileName) {
        InputStream fs;
        XWPFWordExtractor extractor;
        String fileData = null;

        try {
            fs = new FileInputStream(fileName);
            XWPFDocument hdoc = new XWPFDocument(fs);
            extractor = new XWPFWordExtractor(hdoc);
            fileData = extractor.getText();
//            POIXMLProperties.CoreProperties coreProperties = extractor.getCoreProperties();
//            POIXMLProperties.CustomProperties customProperties = extractor.getCustomProperties();
//            POIXMLDocument poixmlDocument = extractor.getDocument();
//            POIXMLProperties.ExtendedProperties extendedProperties = extractor.getExtendedProperties();
//            POIXMLTextExtractor poixmlTextExtractor = extractor.getMetadataTextExtractor();
//            System.out.println("boo");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileData;
    }

    public String getTikaAutoParse(final String fileName) throws IOException, SAXException, TikaException {
        InputStream stream = new FileInputStream(fileName);
        AutoDetectParser parser = new AutoDetectParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        try {
            parser.parse(stream, handler, metadata);
            return handler.toString();
        } finally {
            stream.close();
        }
    }

}
